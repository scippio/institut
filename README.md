## Pirátský institut

Politickým institutem se rozumí právnická osoba, jejímž hlavním předmětem činnosti je výzkumná, publikační, vzdělávací nebo kulturní činnost v oblasti a) rozvoje demokracie, právního státu, pluralismu a ochrany základních lidských práv, b) rozvoje občanské společnosti a společenské soudržnosti, c) podpory aktivní účasti občanů na veřejném životě, d) zlepšení kvality politické kultury a veřejné diskuse, nebo e) přispívání k mezinárodnímu porozumění a spolupráci.

základní pirátské myšlenky je možno najít zde: https://falkvinge.net/pirate-wheel/

__Pirátský institut je prozatím ve fázi pseudohypotetické existence. Další materializace coming soon™.__

### Rozcestník Pirátského institutu

#### IRC:
__server:__ freenode
__channel:__ #chliv

##### klipův pirátský IRC Lounge:
__URL:__ https://irc.pirati.info/
[Stačí si změnit Nick a připojit se.]

#### Vlákno k institutu na pirátském foru:
__URL:__ https://forum.pirati.cz/vnitrostranicka-diskuse-f75/think-tank-aktivisticke-kridlo-neco-v-tom-smyslu-t36707.html
