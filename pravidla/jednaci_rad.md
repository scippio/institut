# Jednací řád Pirátského institutu

## § 1 Jednání

(1) Cílem Jednání je rozhodovat v působnosti orgánu
* a) usnesením které vybírá z předložených návrhů usnesení, nebo
* b) volbou, ve které vybírá z navržených kandidátů

(2) Členové orgánu mají právo za podmínek předpisu podat návrhy, účastnit se rozpravy a hlasovat. Svá práva na jednání vykonává každý přímo a osobně.

## § 2 Předsedající

Předsedající řídí s přihlédnutím ke všeobecné shodě přítomných jednání orgánu, a pokud to řádné jednání vyžaduje, může pořádkovým opatřením odebrat řečníkovi slovo nebo osobu z jednání vykázat.

## § 3 Základní postup

(1) Jednání zahájí pověřený člen jen na žádost, pokud mu přijatelný návrh usnesení předloží _XXX_. Nebo je třeba zvolit členy orgánu, aby byl řádně obsazen, 

(2) Po zahájení jednání se koná k projednávaným návrhům rozprava. Cílem rozpravy je dosáhnout pokud možno všeobecné shody na přijímaném rozhodnutí, případně jeho zlepšení. V rozpravě může se svým projevem vystoupit každý přítomný.

(3) Po ukončení rozpravy začne běžet doba na rozmyšlenou.... TODO

(4) V rozhodující hlasování vybere orgán jednu z následujících možností:
* a) schválení některého návrhu usnesení, který pověřený člen zaznamenal,
* b) ukončení projednávání návrhů usnesení bez přijetí usnesení,
* c) vybere z navržených kandidátů takový počet kandidátů, který může být nejvýše zvolen.

## § 4 Hlasování

(1) Hlasováním se vybírá jedna nebo více možností. Hlasování probíhá pomocí Schulzovy metody vzestupným systémem.

(2) Pověřená osoba předsedáním upozorní bezprostředně před hlasováním všechny přítomné členy na zahájení hlasování. Hlasovací právo náleží členovi, který je při hlasování zapsán v seznamu členů. Přítomným je při hlasování ten, kdo skutečně hlasoval.

(3) Po ukončení hlasování vyhlásí předsedající jeho výsledek.

## § 5 Námitka

(1) Námitka je návrh postupu, kterým celostátní fórum zvažuje návrat k předchozímu stavu při zpochybnění správnosti jeho postupu. Všechny námitky je třeba podat bezprostředně po tom, co nastala namítaná skutečnost.

(2) Pokud celostátní fórum námitce vyhoví, obnovuje se stav před namítanou skutečností a k namítané skutečnosti a následujícímu postupu se nepřihlíží. Jinak celostátní fórum v postupu pokračuje a k další stejné námitce nepřihlíží.

## § 6 Ukončení jednání
Předsedající jednání ukončí, pokud byla projednána všechna rozhodnutí, kvůli nimž bylo jednání zahájeno. Předsedající zveřejní přijatá rozhodnutí na elektronickém systému strany a učiní jiné potřebné úkony pro jeho provedení.
