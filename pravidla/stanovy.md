# Stanovy Pirátského Institutu

## Čl. 1 Základní ustanovení

(1) Název spolku je Pirátský institut z.s. (dále tež „PI“)  

(2) Sídlem PI je Praha.

## Čl. 2 Poslání

(1) Institut staví na myšlenkách pirátství a svou činností je dále rozvíjí.

(2) Institut chce fungovat jako nezávislá platforma podporující tvořivou spolupráci sdílením bezpečného a spravedlivého prostředí, zdrojů a informací.

(3) Institut chce svou činností podporovat a prosazovat zejména:
* a) ochranu soukromí jednotlivce,
* b) transparentní veřejný sektor,
* c) volný tok a sdílení nástrojů, ideálů, kultury, informací a postojů,
* d) nenásílí a humanismus,
* e) různorodost,
* f) stabilitu,
* g) občanskou ekonomiku,
* h) kvalitní legislativu.

## Čl. 3 Činnost

(1) K dosažení svého poslání Pirátský Institut zejména podporuje a sám uskutečňuje výzkumnou a kulturní činnost v oblastech:
* a) rozvoje demokracie, právního státu, pluralismu a ochrany základních lidských práv,
* b) rozvoje občanské společnosti a společenské soudržnosti,
* c) podpory aktivní účasti občanů na veřejném životě,
* d) zlepšení kvality politické kultury a veřejné diskuse,
* e) přispívání k mezinárodnímu porozumění a spolupráci.
* f) ochrany přírody.

## Čl. 4 Členství

(1) Řádným členem PI může být fyzická osoba starší 18 let či právnická osoba, která se ztotožňuje s posláním PI a chce se aktivně podílet na jeho činnosti. Členství v PI vzniká na základě schválení přihlášky Jádrem.

(2) Právnická osoba nemá hlasovací práva a nemůže být volena do orgánů PI.

(3) Každý řádně přijatý člen se stává členem nejvyššího orgánu PI.

(4) Řádný člen PI má právo:
* a) účastnit se veškeré činnosti PI, volit a být volen do orgánů PI.
* b) předkládat návrhy, podněty a připomínky orgánům PI.
* c) být informován o činnosti PI.
* d) na veškeré informace o své osobě, vedené v souvislosti s jeho členstvím,
* e) účastnit se jednání jakéhokoliv orgánu PI, projednává-li tento závažné okolnosti týkající se jeho osoby.

(5) Řádný člen PI má povinnost:
* a) dodržovat znění Stanov a řídit se usneseními a rozhodnutími orgánů PI
* b) platit pravidelné členské příspěvky, jejichž výši, splatnost a způsob platby určuje Kruh na základě
návrhu Jádra
* c) Dodržovat interní předpisy PI, jsou-li vydány, zejména strpět omezení svých práv, které uloží Rozhodčí komise za porušení povinností člena.

(6) Členství v PI zaniká:
* a) Řádným oznámením o vystoupení člena Jádru PI.
* b) Nezaplacením členského příspěvku za daný kalendářní rok ani do třetího měsíce ode dne splatnosti
* c) Vyloučením člena z důvodu neplnění jeho povinností vyplývajících z členství v PI. Člen je oprávněn do patnácti dnů od doručení rozhodnutí v písemné formě navrhnout, aby rozhodnutí o jeho vyloučení přezkoumala Rozhodčí komise. Přezkoumání má odkladný účinek.
* d) Úmrtím člena či zánikem právnické osoby.

(7) Na členství v PI není právní nárok.

## Čl 5. Orgány

(1) Základní orgány PI jsou:
* a) Kruh Pirátského institutu (dále jen „Kruh“),
* b) Jádro Pirátského institutu (dále jen „Jádro“),
* c) Rozhodčí komise (dále jen „RK“),

(2) Volby do volených orgánů PI se uskutečňují veřejným hlasováním, pokud není hlasováním určeno jinak.

(3) Členství ve volených orgánech PI zaniká:
* a) rezignací člena Řádným oznámením,
* b) uplynutím funkčního období,
* c) odvoláním,
* d) zánikem členství v PI.

(4) Orgány jsou usnášeníschopné pokud byly svolány Řádným oznámením nebo při účasti nadpoloviční většiny všech jejich členů. Usnášejí se hlasováním dle Jednacího řádu. [(1)](#note-1)

(5) Pokud počet členů voleného orgánu klesne pod stanovené množství, tak Jádro svolá bezodkladně jednání Kruhu, kde proběhne dovolba.

(6) Z každého jednání voleného orgánu PI se bez zbytečného odkladu pořizuje zápis.

(7) Každý orgán může pověřit jakéhokoliv člena, který se zodpovídá danému orgánu a může plnit předem dané úkoly v rámci pravomocí orgánu jako:
* a) svolávat a řídit jednání orgánu jako Předsedající dle § 2 Jednacího řádu,
* b) jednat jménem orgánu navenek,
* c) a další specificky vyjmenované úkoly.

(8) Základní orgány nelze zrušit.

(9) Pokud není uvedeno jinak, tak se orgány řídí Jednacím řádem PI.

## Čl. 6 Kruh

(1) Kruh je nejvyšším orgánem PI a je tvořen shromážděním všech členů PI.

(2) Kruhu přísluší:
* a) rozhodovat třípětinovou kvalifikovanou většinou přítomných členů o změnách stanov PI,
* b) schvalovat Jednací řád,
* c) schvalovat Pravidla hospodaření,
* d) schvalovat Rozhodčí řád,
* e) schvalovat výroční zprávy o hospodaření PI,
* f) rozhodovat třípětinovou většinou přítomných členů o zrušení PI,
* g) rozhodovat v případě likvidace o jmenování likvidátora,
* h) rozhodovat o odvolání člena proti rozhodnutí RK o jeho vyloučení,
* i) rozhodovat o výši, splatnosti a způsobu platby členských příspěvků,
* j) rozhodovat o počtech členů Jádra na další funkční období, volit a odvolávat členy Jádra.
* k) rozhodovat o navazování spolupráce s tuzemskými i zahraničními organizacemi,
* l) zřizovat a rušit orgány PI, které nejsou základní a stanovovat účel a pravidla jejich činnosti.
* m) rušit rozhodnutí všech orgánů PI

(3) Kruh si může vyhradit právo rozhodnout o jakékoli záležitosti, která není stanovami svěřena do působnosti jiného orgánu.

(4) Jádro svolává jednání Kruhu podle potřeby, nejméně jednou ročně.

(5) Jednáni Kruhu musí být svoláno Řádným oznámením všem členům alespoň 14 dnů přede dnem jeho konáním. Pozvánka musí obsahovat alespoň místo, čas a program jednání. Pozvánka musí být zaslána elektronicky na adresu poskytnutou členem.

(6) Jednání Kruhu se mají právo zúčastnit všichni členové PI. Každý člen, který není právnickou osobou má jeden hlas.

(7) Kruh svolaný v souladu s odst. 5 je usnášeníschopný bez ohledu na počet zúčastněných členů PI.

(8) Kruh rozhoduje hlasováním dle Jednacího řádu PI.

(9) Záležitost, která nebyla zařazena na pořad jednání Kruhu v zaslané pozvánce, lze zařadit na program jednání jen se souhlasem 2/3 přítomných členů Kruhu.

(10) O jednání Kruhu vyhotoví Jádro či pověřená osoba veřejný zápis nejpozději do 14 dnů od jejího konání.

## Čl. 7 Jádro

(1) Jádro je statutárním a výkonným orgánem PI, který za svou činnost odpovídá Kruhu.

(2) Jádro tvoří 3-9 členů. Počet členů Jádra musí být vždy lichý.

(3) Funkční období je 2 roky.

(4) Jednání Jádra svolává kterýkoli člen Jádra dle potřeby, nejméně však jednou za 3 měsíce.

(5) Na žádost jedné třetiny členů Kruhu svolá pověřený člen Jádra jednání Kruhu nejpozději do 2 týdnů od doručení žádosti.

(6) Jádro zejména:
* a) řídí činnost PI,
* b) zpracovává podklady pro rozhodnutí Kruhu,
* c) předkládá Kruhu výroční zprávy o činnosti a hospodaření,
* d) rozhoduje o přijetí za člena PI,
* e) vede seznam členů PI.

(7) Jednání Jádra musí být oznámeno elektronicky na adresu uvedenou v seznamu členů všem členům Jádra alespoň 7 dnů před jeho konáním. Z pozvánky musí být zřejmé místo, čas a program jednání. Ve výjimečných případech je možnost i mimořádného jednání, které se může konat s menším časovým odstupem od oznámení.

## Čl. 8 Přezkumné orgány

(1) Přezkumné orgány jednají podle právních principů běžných v demokratickém soudnictví, zejména dbají na to, aby nikomu nestranily.

(2) Pokud není uvedeno jinak, tak kontrolní činnost vykonávají všichni členové PI.

## Čl. 9 Rozhodčí komise

(1) vyšetřuje stížnosti na porušení předpisů týkajících se činnosti PI.

(2) Rozhodčí komisi tvoří 3 nebo 5 členů.

(3) Ostatní orgány jsou při přezkumu povinny poskytnout RK součinnost.

(4) RK rozhoduje ve sporech PI mezi fyzickými osobami navzájem, resp. mezi fyzickými osobami nebo orgány.

(5) Při projednávání stížností vždy vyslechne účastníky řízení.

(6) Rozhodčí komise se řídí Rozhodčím řádem PI.

(7) Vydává předběžná opatření ve věcech, které nesnesou odkladu, a rozhoduje o omezení práv člena za porušení právních předpisů a o jeho vyloučení.

(8) Odpovídá na předběžné otázky orgánů strany a jednotlivců, pokud není výklad stanov nebo předpisů jasný; taková odpověď působí při aplikaci silou přesvědčivosti.

## Čl. 10 Jednání jménem PI

(1) Právní úkony jménem PI činní statutární orgán nebo jím pověřená osoba

## Čl. 11 Obecné zásady

(1) Hlasování probíhá pokud možno elektronicky.

(2) Za Řádné oznámení se považuje vážné prohlášení doručené všem adresátům, které nevzbuzuje pochyby o věrohodnosti. Úkony nařízené právními předpisy buďtež řádně oznámeny.

(3) Volební období začíná ukončením jednání v kterém proběhla dana volba nebo vyhlášením výsledků volby.

(4) Každý člen může zastávat nejvýše jednu z funkcí Jádra nebo Rozhodčí komise.

(5) Na funkci v orgánu vykonávanou podle těchto stanov může fyzická osoba rezignovat Řádným oznámením.

(6) Každé jednání musí být svoláno Řádným oznámením, jinak není usnášeníschopné.

## Čl. 12 Přechodná ustanovení

(1) Prvními členy PI se stávají zakladatelé PI, kteří se shodli na stanovách PI.

(2) Prvními členy Jádra, jakožto statutárního orgánu jsou JMÉNA ČLENŮ.

(3) Funkční období prvních členů Jádra je pouze 1 rok.

## Čl. 13 Závěrečná ustanovení

(1) V rámci svých pravomocí daných těmito stanovami mohou jednotlivé orgány PI vydávat závazné interní předpisy.

(2) Tyto stanovy nabývají platnosti a účinnosti dnem jejich schválení.

---

##### 1. <a name="note-1"></a> Orgán >> Usnášeníschopnost: Řádné oznámení nebo nadpoloviční účast + Hlasování: nadpoloviční přítomní (pokud Řádné oznámení)
