# Rozhodčí řád Pirátského institutu

## § 1 Účel předpisu

(1) Rozhodčí řád upravuje rozhodčí řízení ve sporech vedených v souladu se stanovami před rozhodčí komisí.

(2) ... _TODO_

## § 2 ..TODO

## § 3 Rozhodné právo

Rozhodným právem jsou obecně závazné předpisy účinné v České republice, vnitřní předpisy PI a v jejich mezích také zavedené právní zvyklosti a zásady spravedlnosti. Rozhodné právo se posuzuje podle okamžiku, kdy bylo porušení předpisů dokonáno, není-li stanoveno jinak.

## § 4 Cíl rozhodčího řízení
(1) Cílem rozhodčího řízení je rozhodnout spor uvnitř PI rozhodčím nálezem, ve kterém Rozhodčí komise samostatnými výroky
* a) vysloví, zda došlo k porušení předpisů (§ 5),
* b) určí, kdo za porušení předpisů nese jakou odpovědnost (§ 6)

## § 5 Porušení předpisů

(1) Porušením předpisů ve smyslu rozhodčího řádu je jednání nebo opomenutí, které je v rozporu s rozhodným právem a které se týká činnosti PI. Porušením předpisů je také jejich zjevné obcházení nebo vědomé udržování protiprávního stavu.


## § 6 Náprava

(1) Náprava se zjedná bezodkladným uvedením do souladu s rozhodným právem, náhradou způsobené újmy, omluvou, přiměřeným zadostiučiněním v penězích nebo také jiným způsobem, kterým je újma spravedlivě odčiněna.

## § 7 Omezení práv

## § 8 Vyloučení z PI
