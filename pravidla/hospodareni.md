# Hospodaření Pirátského institutu

## § 1 Účel předpisu

Předpis popisuje přesný postup při příjmání a nakládání s prostředky PI.

## § 2 

(1) Za hospodaření PI zodpovídá statutární orgán, který každoročně předkládá Kruhu zprávu o hospodaření.

(2) O svém hospodaření vede PI účetnictví podle platných právních předpisů a interních předpisů PI.

(3) Všechny příjmy, výdaje a prostředky jsou soustředěny v jediném systému, který je veřejným informačním systémem spolku vedený jedním či více pověřenými členy.

(4) Prostředky PI jsou děleny na volné a vázané.

(5) Ve veřejném informačním systému PI se dále zveřejňují:
* a) faktury, objednávky a doklady na výdaje,
* b) výroční finanční zprávy,
* c) účetní výkazy podle zákona o účetnictví,
* d) projekty a jejich plnění včetně žádostí o proplacení,
* e) veškeré smlouvy,
* f) soupis věcí, které spravuje každý projekt, s jejich hodnotou a místem výskytu,
* g) seznam transparentních bankovních účtů,
* h) seznam jiných finančních účtů s přehledem transakcí na nich.

(6) PI hospodaří elektronicky, není-li stanoveno jinak. Jako bankovní účty PI používá pouze transparentní bankovní účty ve správě pověřených členů. Stavy a transakce na bankovních účtech institutu zveřejňuje banka na internetové stránce.

(7) Jádro PI pověří dva členy správou bankovního účtu. Správcovský přístup k účtu PI má pouze tento pověřený člen.

(8) Osoba, která má získat jakákoliv přístupová oprávnění k bankovnímu účtu PI, musí před zapsáním tohoto oprávnění u banky podepsat dohodu o hmotné odpovědnosti za neoprávněné převody provedené pod svými uživatelskými údaji a musí být poučena o bezpečnostních pravidlech. Za prověření důvěryhodnosti a spolehlivosti odpovídá Jádro.

(9) Pověřený člen převádí peníze z bankovního účtu na žádost o proplacení podanou správcem projektu.

(10) Žádost o proplacení musí obsahovat informaci o druhu výdaje podle třídění stanoveného pověřenou osobou zodpovídající za převody prostředků.

(11) Před rozhodnutím o žádosti zkoumá pověřená osoba zodpovídající za převody prostředků, zda výdaj odpovídá pravidlům hospodaření, zejména
* a) zda byl výdaj schválen správcem projektu, který odpovídá za jeho výdaje,
* b) zda je výdaj součástí záměru, který je odsouhlasen u projektu, případně zda je připojen souhlas dle podmínek projektu,
* c) zda byla projekt schválen a má dostatek vázných prostředků pro proplacení,
* d) zda byl výdaj hodnověrně doložen, pokud je proplácen zpětně,
* e) zda je na příslušném bankovním účtu dostatek prostředků pro proplacení výdaje.

(12) Pověřený člen zamítne žádost, která je v rozporu s pravidly hospodaření. Jinak pověřený člen žádosti vyhoví a žádost proplatí zpětně nebo dopředu na účet příjemce bez zbytečného odkladu.

## § 3 Příjmy

(1) Příjmy se dělí na Bezúčelné a Účelově určené.

(2) Účelově určené příjmy se automaticky přidělí konkrétnímu projektu při splnění čl.__Projekt__ odst. 7.bankov

## § 4 Volné prostředky

(1)

(2) Volné prostředky jsou rovným dílem děleny na části podle aktuálního počtu členů. Každý člen má možnost pomocí hlasování přidělit prostředky ve výši jednoho dílu příslušnému projektu dle čl. __Projekt__.

(3) Přidělené volné prostředky projektu se stávají vázanými.

## § 4 Vázané prostředky

(1) Nevyužité prostředky v projektu (například jeho ukončením, či zrušením) se vrací zpět do volných prostředků.

## § 5 Projekt

(1) Projekt je soubor věcných, časových, osobních a finančních podmínek pro činnost potřebnou k dosažení cíle který Kruh v hlasování schválí.

(2) Každý projekt má určeného správce. Správce se určí ještě před hlasováním a lze ho měnit pouze novým hlasováním.

(3) Projekt může být zrušen rozhodnutím Kruhu.

(4) Popis každého projektu musí před svým schválením obsahovat:
* a) název projektu,
* b) krátký srozumitelný popis projektu,
* c) označení správce,
* d) věcné podmínky projektu (vybavení k řešení projektu, které bude nutné zakoupit nebo se souhlasem jeho správce používat),
* e) časové podmínky projektu (termín zahájení a řádného ukončení projektu, členění projektu na úseky, odhadovaný celkový čas strávený prací na projektu),
* f) osobní podmínky projektu (označení osob, které přislíbily věnovat svůj čas projektu)
* g) finanční podmínky projektu (minimální a maximální výše prostředků na projekt či úseky, při jejichž získání bude projekt řešen, orientační rozpočet projektu, další zdroje financování, výše odměny pro řešitele nebo jiné osoby, pokud je navrhována),
* h) upřesnění způsobu a termínu, kdy bude výsledek projektu zveřejněn.
  
(5) Popis projektu může definovat velikost částky přpadající na jeden hlas, který v hlasování určuje výši poskytnutých prostředků.

(6) Výše prostředků, kterou určitý projekt získal v hlasování, se určí jako násobek počtu hlasů pro něj odevzdaných a částky připadající na jeden hlas.

(7) Výše prostředků přidělená projektu nesmí přesáhnout jejich maximální výši, pokud je projektem určena. Pokud je maximální výše prostředků překročena, vrací se přesahující částka zpět do volných prostředků PI.

## § 6 Správce projektu

(1) Správce projektu je povinen každý výdaj řádně doložit ve stanovené lhůtě dokladem podle zákona. PI není zavázán uhradit náklady schváleného výdaje, pokud doklad neobdrží ani v dodatečně určené lhůtě.
